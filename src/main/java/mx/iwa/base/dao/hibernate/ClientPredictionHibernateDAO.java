package mx.iwa.base.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.iwa.base.dao.ClientPredictionDAO;
import mx.iwa.base.domain.ClientPrediction;

@Repository
public class ClientPredictionHibernateDAO extends AbstractGenericDAO<ClientPrediction, Integer> implements ClientPredictionDAO {
	
	@Autowired
	public ClientPredictionHibernateDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ClientPrediction> getAll() {
		return (List<ClientPrediction>) getSession().createCriteria(getDomainClass()).list();
	}

}
