package mx.iwa.base.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.iwa.base.dao.ClientDAO;
import mx.iwa.base.domain.Client;

@Repository
public class ClientHibernateDAO extends AbstractGenericDAO<Client, Integer> implements ClientDAO {
	
	@Autowired
	public ClientHibernateDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Client> getAll() {
		return (List<Client>) getSession().createCriteria(getDomainClass()).list();
	}

}
