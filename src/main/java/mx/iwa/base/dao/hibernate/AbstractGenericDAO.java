package mx.iwa.base.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import mx.iwa.base.dao.AbstractDAO;

public class AbstractGenericDAO<T extends Serializable, KeyType extends Serializable> implements AbstractDAO<T, KeyType> {
	
	private Class<T> domainClass;	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public AbstractGenericDAO(SessionFactory sessionFactory) {		
		this.sessionFactory = sessionFactory;
		if (getClass().getGenericSuperclass() instanceof Class) {
			this.domainClass = (Class<T>) getClass().getGenericSuperclass();
	    } else if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
	    	this.domainClass = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	    }
	}
	
	public Class<T> getDomainClass() {
		return domainClass;
	}
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public Criteria getCriteria(){
		return getSession().createCriteria(getDomainClass());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findById(KeyType id){
		return (T)getSession().get(domainClass, id);
	}
	
	@Override
	public void persist(T domainClass){
		getSession().persist(domainClass);
	}
	
	@Override
	public void update(T domainClass){
		getSession().merge(domainClass);
	}
	
	@Override
	public void delete(T domainClass){
		getSession().delete(domainClass);
	}

	@Override
	public void flush() {
		getSession().flush();
	}
	
}