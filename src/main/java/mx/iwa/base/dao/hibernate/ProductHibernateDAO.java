package mx.iwa.base.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.iwa.base.dao.ProductDAO;
import mx.iwa.base.domain.Product;

@Repository
public class ProductHibernateDAO extends AbstractGenericDAO<Product, Integer> implements ProductDAO {
	
	@Autowired
	public ProductHibernateDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Product> getAll() {
		return (List<Product>) getSession().createCriteria(getDomainClass()).list();
	}

}
