package mx.iwa.base.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.iwa.base.dao.ProductClientDAO;
import mx.iwa.base.domain.ProductClient;

@Repository
public class ProductClientHibernateDAO extends AbstractGenericDAO<ProductClient, Integer> implements ProductClientDAO {
	
	@Autowired
	public ProductClientHibernateDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ProductClient> getAll() {
		return (List<ProductClient>) getSession().createCriteria(getDomainClass()).list();
	}

}
