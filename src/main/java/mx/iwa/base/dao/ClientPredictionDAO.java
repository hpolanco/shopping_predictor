package mx.iwa.base.dao;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import mx.iwa.base.domain.ClientPrediction;

@Validated
public interface ClientPredictionDAO extends AbstractDAO<ClientPrediction, Integer> {
	
	List<ClientPrediction> getAll();

}
