package mx.iwa.base.dao;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import mx.iwa.base.domain.Client;

@Validated
public interface ClientDAO extends AbstractDAO<Client, Integer> {
	
	List<Client> getAll();

}
