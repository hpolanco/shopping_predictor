package mx.iwa.base.dao;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import mx.iwa.base.domain.Product;

@Validated
public interface ProductDAO extends AbstractDAO<Product, Integer> {
	
	List<Product> getAll();

}
