package mx.iwa.base.dao;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import mx.iwa.base.domain.ProductClient;

@Validated
public interface ProductClientDAO extends AbstractDAO<ProductClient, Integer> {
	
	List<ProductClient> getAll();

}
