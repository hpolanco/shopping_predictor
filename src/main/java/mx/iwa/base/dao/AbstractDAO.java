package mx.iwa.base.dao;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

@Validated
public interface AbstractDAO<DomainObject, KeyType> {
	
	DomainObject findById(@NotNull KeyType id);
	void persist(@NotNull DomainObject domainObject);
	void update(@NotNull DomainObject domainObject);
	void delete(@NotNull DomainObject domainObject);
	void flush();
	
}
