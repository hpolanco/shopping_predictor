package mx.iwa.base.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "product_client")
@DynamicUpdate
@SuppressWarnings("serial")
public class ProductClient implements Serializable {
	
	private Integer id;
	private Client client;
	private ProductClient productClient;
	private Date actualDate;
	private Date lastDate;
	private Integer numberDays;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_client_product", nullable  = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_client", nullable = false)
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_product", nullable = false)
	public ProductClient getProductClient() {
		return productClient;
	}

	public void setProductClient(ProductClient product) {
		this.productClient = product;
	}

	@Column(name = "actual_date", nullable  = false)
	public Date getActualDate() {
		return actualDate;
	}

	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}

	@Column(name = "last_date", nullable  = false)
	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	@Column(name = "number_days", nullable  = false)
	public Integer getNumberDays() {
		return numberDays;
	}

	public void setNumberDays(Integer numberDays) {
		this.numberDays = numberDays;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductClient other = (ProductClient) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("ProductClient [id=");
		builder.append(id);
		builder.append(", actualDate=");
		builder.append(actualDate);
		builder.append(", lastDate=");
		builder.append(lastDate);
		builder.append(", numberDays=");
		builder.append(numberDays);
		builder.append("]");
		
		return builder.toString();
	}
	
}
