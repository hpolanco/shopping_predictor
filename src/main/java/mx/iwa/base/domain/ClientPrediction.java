package mx.iwa.base.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "client_prediction")
@DynamicUpdate
@SuppressWarnings("serial")
public class ClientPrediction implements Serializable {
	
	private Integer id;
	private Product idProductClient;
	private String productAverage;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_prediction", nullable  = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_product_client", nullable  = false)
	public Product getIdProductClient() {
		return idProductClient;
	}

	public void setIdProductClient(Product idProductoClient) {
		this.idProductClient = idProductoClient;
	}

	@Column(name = "product_average", nullable  = false)
	public String getProductAverage() {
		return productAverage;
	}

	public void setProductAverage(String productAverage) {
		this.productAverage = productAverage;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientPrediction other = (ClientPrediction) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("ClientPrediction [id=");
		builder.append(id);
		builder.append(", idProductClient=");
		builder.append(idProductClient);
		builder.append(", productAverage=");
		builder.append(productAverage);
		builder.append("]");
		
		return builder.toString();
	}

}
