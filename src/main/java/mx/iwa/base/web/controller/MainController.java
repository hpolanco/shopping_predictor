package mx.iwa.base.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.iwa.base.dao.ClientDAO;
import mx.iwa.base.domain.Client;
import mx.iwa.base.dto.ShoppingListDTO;
import mx.iwa.base.dto.TestEntityDTO;
import mx.iwa.base.service.ClientService;

@Controller
@RequestMapping(path = "/predictor")
public class MainController {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);
	
	@Autowired private ClientService testEntityDAO;
	
	@RequestMapping(path = "/shopping/add", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Boolean> addShoppingList(@RequestBody ShoppingListDTO request) {
		return new ResponseEntity<Boolean>(HttpStatus.CONFLICT);
	}
	
	@RequestMapping(path = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<TestEntityDTO> getSimpleAnswer() {
		
		//Client entity = testEntityDAO.getClient(1);
		TestEntityDTO dto = new TestEntityDTO();
		dto.setId(1);
		dto.setName("Carlos");
		
		return new ResponseEntity<TestEntityDTO>(dto, HttpStatus.OK);
	}
	
	

}
