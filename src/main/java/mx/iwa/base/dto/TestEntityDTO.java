package mx.iwa.base.dto;

import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class TestEntityDTO implements Serializable {
	
	private Integer id;
	private String name;

}
