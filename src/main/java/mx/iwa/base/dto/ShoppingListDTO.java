package mx.iwa.base.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class ShoppingListDTO implements Serializable {
	
	private Date date;
	private List<ProductDTO> products;

}
