package mx.iwa.base.dto;

import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class ProductDTO implements Serializable {
	
	private String name;
	private String amount;

}
