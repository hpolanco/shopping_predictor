package mx.iwa.base.service;

import mx.iwa.base.dao.ClientDAO;
import mx.iwa.base.domain.Client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService {
	
	@Autowired private ClientDAO clientDAO;

	@Transactional(readOnly = true)
	public Client getClient(Integer id){
		return clientDAO.findById(id);
	}
	
}
